package com.okupyd.android.okupyd.activity;

import android.graphics.Color;
import android.nfc.Tag;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.okupyd.android.okupyd.R;
import com.okupyd.android.okupyd.models.Building;
import com.okupyd.android.okupyd.models.Room;
import com.okupyd.android.okupyd.models.Seat;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.view.PieChartView;

public class ClientListActivity extends AppCompatActivity {


    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myRoomRef = database.getReference("rooms");
    private DatabaseReference mySeatsRef = database.getReference("seats");
    private DatabaseReference myBuildingRef = database.getReference("building");
    private String buildingID = "ChIJSXFbmjAJZ0gRtYltUXsp3YA";
    private String TAG = "Eoin";

    private PieChartView pieChartView;
    private TextView tv1;
    private TextView tv2;
    private TextView tv3;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_list);

        /* Original Android Programming */

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });


        //MY CODE


        this.writeToDatabase();
        this.readfromDatabase();

        pieChartView = findViewById(R.id.chart);

        List<SliceValue> pieData = new ArrayList<>();
        pieData.add(new SliceValue(15, Color.BLUE).setLabel("Q1: $10"));
        pieData.add(new SliceValue(25, Color.GRAY).setLabel("Q2: $4"));
        pieData.add(new SliceValue(10, Color.RED).setLabel("Q3: $18"));
        pieData.add(new SliceValue(60, Color.MAGENTA).setLabel("Q4: $28"));

        PieChartData pieChartData = new PieChartData(pieData);
        pieChartData.setHasLabels(true).setValueLabelTextSize(14);
        pieChartData.setHasCenterCircle(true).setCenterText1("Sales in million").setCenterText1FontSize(20).setCenterText1Color(Color.parseColor("#0097A7"));
        pieChartView.setPieChartData(pieChartData);




    }
    //METHODS

    public void writeToDatabase(){
        //SET BUILDING
        Building building = new Building("UCD School of Computer Science", buildingID, 53.309270, -6.223883
        );
        myBuildingRef.child(buildingID).setValue(building);


        //SET ROOMS
        Room room = new Room("A","B108", 2);
        Room room1 = new Room("B","B109", 2);
        myRoomRef.child(buildingID).child(room.getId()).setValue(room);
        myRoomRef.child(buildingID).child(room1.getId()).setValue(room1);

        //SET SEATS
        ArrayList<Seat> seats = new ArrayList<>();
        Seat seat2 = new Seat("2");
        seat2.setOccupied(1);
        seats.add(new Seat("1"));
        seats.add(seat2);

        for(Seat seat: seats){
            mySeatsRef.child(buildingID).child(room.getId()).child(seat.getId()).setValue(seat);
            mySeatsRef.child(buildingID).child(room1.getId()).child(seat.getId()).setValue(seat);
        }

    }

    public void readfromDatabase(){


        // Read from the database
        myBuildingRef.child(buildingID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.

                Log.d(TAG, "Datasnapshot: " + dataSnapshot.getValue().toString());

                Building building = (Building) dataSnapshot.getValue(Building.class);
                    Log.d(TAG, "Value is: " + building.getName());
                    Toast toast = Toast.makeText(getApplicationContext(), "here"+building.getName(), Toast.LENGTH_LONG);
                    toast.show();


//                Log.d(TAG, "Value is: " + value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
//                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }
}
