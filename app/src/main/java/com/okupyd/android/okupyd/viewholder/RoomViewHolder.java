package com.okupyd.android.okupyd.viewholder;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.okupyd.android.okupyd.R;
import com.okupyd.android.okupyd.activity.RoomActivity;
import com.okupyd.android.okupyd.models.Room;

import java.util.ArrayList;

public class RoomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView room_name;
    private TextView number_of_seats;
    private ArrayList<Room> rooms;
    private Context context;
    private String ROOM_ID = "room_id";


    public RoomViewHolder(View itemView, ArrayList<Room> rooms, Context context) {
        super(itemView);
        itemView.setOnClickListener(this);


        room_name = (TextView) itemView.findViewById(R.id.room_name);
        number_of_seats = (TextView) itemView.findViewById(R.id.number_of_seats);

        this.rooms = rooms;

        this.context = context;

    }

    @Override
    public void onClick(View view) {
        int position = getAdapterPosition();
        Room currentRoom = this.rooms.get(position);
        Intent roomDetailsIntent = new Intent(context, RoomActivity.class);
        roomDetailsIntent.putExtra(ROOM_ID, currentRoom);
        this.context.startActivity(roomDetailsIntent);
    }


    public TextView getRoom_name() {
        return room_name;
    }

    public TextView getNumber_of_seats() {
        return number_of_seats;
    }

}
