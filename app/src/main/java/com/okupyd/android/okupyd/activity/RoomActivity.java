package com.okupyd.android.okupyd.activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.okupyd.android.okupyd.R;
import com.okupyd.android.okupyd.models.Room;
import com.okupyd.android.okupyd.models.Seat;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.view.PieChartView;

public class RoomActivity extends AppCompatActivity {

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myRoomRef = database.getReference("rooms");
    private DatabaseReference mySeatsRef = database.getReference("seats");
    private DatabaseReference myBuildingRef = database.getReference("building");
    private String buildingID = "ChIJSXFbmjAJZ0gRtYltUXsp3YA";
    private String TAG = "Eoin";
    private String ROOM_ID = "room_id";


    private PieChartView pieChartView;
    private TextView tv1;
    private TextView tv2;
    private TextView tv3;

    private Room thisRoom;
    private ArrayList<Seat> seats;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);

        pieChartView = findViewById(R.id.chart);
        this.getDetails();
        this.getSeats();
    }

    private void getDetails(){
        tv1 = (TextView) findViewById(R.id.room_object_name);
        tv2 = (TextView) findViewById(R.id.num_of_seats);
        tv3 = (TextView) findViewById(R.id.percentage_occupied);
        Intent intent = getIntent();
        thisRoom =(Room) intent.getSerializableExtra(ROOM_ID);
        tv1.setText(thisRoom.getName());

    }

    private void getSeats(){

        mySeatsRef.child(buildingID).child(thisRoom.getId()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                seats = new ArrayList<>();
                for(DataSnapshot ds : dataSnapshot.getChildren()){
                    Seat seat = ds.getValue(Seat.class);
                    seats.add(seat);
                }
                getPercentage();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void setTextViews(double occupiedPercentage){
        tv2.setText(seats.size()+" seats");
        tv3.setText(occupiedPercentage+"% occupied");
    }

    private void getPercentage(){
        double occupied = 0.0;
        double occupiedDecimal = 0.0;
        int occupiedPercentage = 0;
        int remainder = 0;
        Log.d(TAG, "seatsSize: "+seats.size());
        for(Seat s: seats){
            occupied += s.getOccupied(); //Calculate how many seats are occupied
        }
        Log.d(TAG, "Occupied: "+occupied);
        occupiedDecimal = occupied / seats.size(); //Get a percentage based on the amount of seats
        Log.d(TAG, "OccupiedDecimal: "+occupiedDecimal);
        double temp = 100 * occupiedDecimal;
        occupiedPercentage = (int) temp;
        Log.d(TAG, "OccupiedDecimal: "+occupiedPercentage);
        remainder = 100 - occupiedPercentage;
        Log.d(TAG, "remainder: "+remainder);

        this.setTextViews(occupiedPercentage);

        List<SliceValue> pieData = new ArrayList<>();
//        pieData.add(new SliceValue(15, Color.BLUE).setLabel("Q1: $10"));
//        pieData.add(new SliceValue(25, Color.GRAY).setLabel("Q2: $4"));
//        pieData.add(new SliceValue(10, Color.RED).setLabel("Q3: $18"));
//        pieData.add(new SliceValue(60, Color.MAGENTA).setLabel("Q4: $28"));

        pieData.add(new SliceValue(occupiedPercentage, Color.RED).setLabel("Occupied"));
        pieData.add(new SliceValue(remainder, Color.GREEN).setLabel("Unoccupied"));

        PieChartData pieChartData = new PieChartData(pieData);
        pieChartData.setHasLabels(true).setValueLabelTextSize(14);
//        pieChartData.setHasCenterCircle(true).setCenterText1("Sales in million").setCenterText1FontSize(20).setCenterText1Color(Color.parseColor("#0097A7"));
        pieChartView.setPieChartData(pieChartData);
    }

}
