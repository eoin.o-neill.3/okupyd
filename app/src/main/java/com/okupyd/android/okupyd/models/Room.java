package com.okupyd.android.okupyd.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;

public class Room implements Serializable {

    private String id;
    private int seats;
    private String name;

    public Room(){};

    public Room(String id, String name, int numOfSeats){
        this.id = id;
        this.seats = numOfSeats;
        this.name = name;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    //GETTERS AND SETTERS0
    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
