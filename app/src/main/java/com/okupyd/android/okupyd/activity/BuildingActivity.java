package com.okupyd.android.okupyd.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.okupyd.android.okupyd.R;
import com.okupyd.android.okupyd.adapter.RoomAdapter;
import com.okupyd.android.okupyd.models.Building;
import com.okupyd.android.okupyd.models.Room;

import java.util.ArrayList;

public class BuildingActivity extends AppCompatActivity {

    private TextView building_name;
    private String id, name;
    private ArrayList<Room> rooms = new ArrayList<>();

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myBuildingRef = database.getReference("building");
    private DatabaseReference myRoomRef = database.getReference("rooms");
    private DatabaseReference mySeatsRef = database.getReference("seats");
    private String TAG = "Eoin";

    private RecyclerView room_list;
    private RoomAdapter roomAdapter;
    private  Activity activity = this;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_building);
        intent = getIntent();
        activity.setTitle(intent.getStringExtra("BUILDING_NAME"));

        room_list = (RecyclerView) findViewById(R.id.room_list);
        room_list.setLayoutManager(new LinearLayoutManager(this));

        this.getID();
        this.getRooms();
        Toast.makeText(this, "Select a room to view the its occupancy!", Toast.LENGTH_LONG).show();
    }

    private void getRooms() {
        myRoomRef.child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot ds: dataSnapshot.getChildren()){

                    Room room = ds.getValue(Room.class);
                    rooms.add(room);
                }
                roomAdapter = new RoomAdapter(getApplicationContext(), rooms);
                room_list.setAdapter(roomAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    private void getID(){
        building_name = (TextView) findViewById(R.id.building_name);
        name = intent.getStringExtra("BUILDING_NAME");
        id = intent.getStringExtra("BUILDING_ID");
        building_name.setText(name);
    }
}
