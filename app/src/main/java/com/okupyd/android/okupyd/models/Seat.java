package com.okupyd.android.okupyd.models;

import java.sql.Time;
import java.util.UUID;

public class Seat {

    private String id;
    private int occupied = 0;

    public Seat(){};

    public Seat(String id){
        this.id = id ;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public int getOccupied() {
        return occupied;
    }

    public void setOccupied(int occupied) {
        if(occupied == 1){
            this.occupied = occupied;
        } else if ( occupied == 0){
            this.occupied = occupied;
        }
    }

}
