package com.okupyd.android.okupyd.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.okupyd.android.okupyd.R;
import com.okupyd.android.okupyd.models.Building;
import com.okupyd.android.okupyd.models.Room;
import com.okupyd.android.okupyd.viewholder.RoomViewHolder;

import java.util.ArrayList;

public class RoomAdapter extends RecyclerView.Adapter<RoomViewHolder> {

    private Context context;
    private ArrayList<Room> rooms;
    private Building building;


    public RoomAdapter(Context context, ArrayList<Room> rooms){
        this.context = context;
        this.rooms = rooms;
        this.building = building;
    }

    @NonNull
    @Override
    public RoomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.list_room_item, null);
        return new RoomViewHolder(view, rooms, context);
    }

    @Override
    public void onBindViewHolder(@NonNull RoomViewHolder holder, int position) {
        Room room = rooms.get(position);
        holder.getRoom_name().setText("Room: "+room.getName());
        holder.getNumber_of_seats().setText("Number of Seats: "+room.getSeats());
    }

    @Override
    public int getItemCount() {
        return rooms.size();
    }
}
